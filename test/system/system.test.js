/* eslint-env mocha */
'use strict'

const assert = require('assert')
const { request } = require('gaxios')
const childProcess = require('child_process')
const { EOL } = require('os')
const path = require('path')
const { Storage } = require('@google-cloud/storage')
const { createHmac } = require('node:crypto')
const { readFileSync } = require('fs')
const { getLogs } = require('./log-helper.js')

const key = process.env.HMAC_SECRET
if (!key) {
  throw Error('Please provide HMAC signign key with env variable HMAC_SECRET')
}

const FUNCTION_URL =
  'https://us-east1-glsec-video-scanner-dev.cloudfunctions.net/websub-api'

const PROJECT_ID = 'glsec-video-scanner-dev'
const VIDEO_BUCKET = `${PROJECT_ID}-videos-unfiltered`
const TEXT_BUCKET = `${PROJECT_ID}-videos-unfiltered-text-extract`

describe('system tests', function () {
  this.timeout('45m') // each individual test must complete in 45 minutes

  const fixturesPath = '../../functions/websub-api/test/fixtures/'
  const testVideos = [
    {
      id: 'wK693L7plOM',
      requestFile: path.join(fixturesPath, 'post-body1.xml'),
      expectAlert: false
    },
    {
      id: 'OvVYZul9vAw',
      requestFile: path.join(fixturesPath, 'post-body2.xml'),
      expectAlert: true
    }
  ]

  before(async () => {
    const stdout = childProcess
      .execSync('gcloud run services list')
      .toString()
      .split(EOL)

    const fnExists = (line, fn) => line.indexOf(fn) !== -1
    const ok = ['secret-matcher', 'video-fetcher', 'websub-api'].every(fn =>
      stdout.some(line => fnExists(line, fn))
    )

    if (!ok) {
      throw new Error(
        'Cloud function missing or not active. ' +
          'Make sure to deploy all functions before running this test.'
      )
    }

    const storage = new Storage({ projectId: PROJECT_ID })
    const deleteFiles = async bucketName => {
      const bucket = storage.bucket(bucketName)
      const filesProms = testVideos.map(x =>
        bucket.getFiles({ matchGlob: `yt_unfiltered_${x.id}.mp4*` })
      )
      const files = (await Promise.all(filesProms)).flat(3)
      console.log(`deleting files ${files.map(f => f.metadata.id).join(', ')}`)
      await Promise.all(files.map(x => x.delete()))
    }
    await deleteFiles(VIDEO_BUCKET)
    await deleteFiles(TEXT_BUCKET)
  })

  const sign = message =>
    'sha1=' + createHmac('sha1', key).update(message).digest('hex')

  it('does not alert when scanning a video without secret', async () => {
    const vid = testVideos.filter(x => x.id === 'wK693L7plOM').pop()

    const expectedLogEntry = new RegExp(`No secret.*${vid.id}`)
    const logsPromise = getLogs(expectedLogEntry)

    const body = readFileSync(vid.requestFile)
    await request({
      url: FUNCTION_URL,
      method: 'POST',
      headers: {
        'content-type': 'application/atom+xml',
        'x-hub-signature': sign(body)
      },
      data: Buffer.from(body)
    })

    console.log('Tailing log:')
    const entry = await logsPromise
    assert.match(entry.data, expectedLogEntry)
  })

  it('does alert when it finds a secret', async () => {
    const vid = testVideos.filter(x => x.id === 'OvVYZul9vAw').pop()
    const body = readFileSync(vid.requestFile)
    await request({
      url: FUNCTION_URL,
      method: 'POST',
      headers: {
        'content-type': 'application/atom+xml',
        'x-hub-signature': sign(body)
      },
      data: Buffer.from(body)
    })

    const expectedLogEntry = new RegExp(`SECRET FOUND.*${vid.id}`)
    const entry = await getLogs(expectedLogEntry)
    assert.match(entry.data, expectedLogEntry)
  })
})
