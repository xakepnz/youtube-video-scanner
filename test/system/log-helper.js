'use strict'

const { Logging } = require('@google-cloud/logging')
const logging = new Logging()
const logName =
  'projects/glsec-video-scanner-dev/logs/run.googleapis.com%2Fstdout'
const log = logging.log(logName)

async function getLogs (searchRegex) {
  return new Promise((resolve, reject) => {
    const stream = log
      .tailEntries({
        filter:
          '(resource.type = "cloud_run_revision"\n' +
          'resource.labels.service_name = "secret-matcher"\n' +
          'resource.labels.location = "us-east1")\n' +
          'severity>=DEFAULT\n' +
          'timestamp > "2023-09-07T10:13:09.831Z"'
      })
      .on('error', console.error)
      .on('data', resp => {
        for (const entry of resp.entries) {
          console.log('  ' + JSON.stringify(entry.data, null, 4))
          if (searchRegex.test(entry.data)) {
            resolve(entry)
            stream.end()
          }
        }
      })
      .on('end', () => {
        console.log('log entry stream has ended')
        reject(new Error('No string matched'))
      })
  })
}

if (require.main === module) {
  // we are being called from the terminal
  getLogs(/No secret.*wK693L7plOM_136/)
}

module.exports = { getLogs }
