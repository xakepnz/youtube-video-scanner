# frozen_string_literal: true

require 'google/apis/youtube_v3'
require 'net/http'
require 'openssl'
require 'uri'
require 'optparse'
require 'json'

YOUTUBE_BASE_URL = 'https://www.googleapis.com/youtube/v3/'
CHANNEL_ID = 'UCMtZ0sc1HHNtGGWZFDRTh5A' # This is the GitLab Unfiltered channel ID
VIDEO_IDS_FILE = 'video_ids.json'
CACHE_FILE = 'cache.json'

def fetch_videos(low_boundary = nil, top_boundary = nil, refresh_cache = false)
  youtube = Google::Apis::YoutubeV3::YouTubeService.new
  youtube.key = ENV['YOUTUBE_API_KEY']
  cache_file = 'cache.json'

  if File.exist?(cache_file) && !refresh_cache
    puts 'Loading videos from cache...'
    videos = JSON.parse(File.read(cache_file))

    return low_boundary && top_boundary ? videos.slice(low_boundary..top_boundary) : videos
  end

  videos = []
  next_page_token = nil

  loop do
    response = youtube.list_searches('snippet', channel_id: CHANNEL_ID, max_results: 50, page_token: next_page_token,
                                                order: 'date')

    response_videos = response.items.map do |item|
      {
        'title' => item.snippet.title,
        'id' => item.id.video_id
      }
    end
    videos += response_videos
    response_videos.each do |video|
      # puts "Found video with title #{video['title']} and id #{video['id']}"
    end

    next_page_token = response.next_page_token

    break if next_page_token.nil?
  end
  puts "Found #{videos.count} videos"
  videos.reverse!
  update_cache(cache_file, videos)

  low_boundary && top_boundary ? videos.slice(low_boundary..top_boundary) : videos
end

def update_cache(cache_file, new_videos)
  existing_videos = []

  existing_videos = JSON.parse(File.read(cache_file)) if File.exist?(cache_file)
  existing_video_ids = existing_videos.map { |video| video['id'] }
  unique_new_videos = new_videos.reject { |video| existing_video_ids.include?(video['id']) }
  puts "New videos found: #{unique_new_videos}"
  combined_videos = existing_videos + unique_new_videos

  File.write(cache_file, JSON.dump(combined_videos))
end

def send_video_to_ocr(video_id)
  body_xml =
    ''"<?xml version='1.0' encoding='UTF-8'?>
<feed xmlns:yt=\"http://www.youtube.com/xml/schemas/2015\" xmlns=\"http://www.w3.org/2005/Atom\">
  <link rel=\"hub\" href=\"https://pubsubhubbub.appspot.com\"/>
  <link rel=\"self\" href=\"https://www.youtube.com/xml/feeds/videos.xml?channel_id=#{CHANNEL_ID}\"/>
  <title>YouTube video feed</title>
  <updated>2023-07-17T18:32:12.167884102+00:00</updated>
  <entry>
    <id>yt:video:#{video_id}</id>
    <yt:videoId>#{video_id}</yt:videoId>
    <yt:channelId>#{CHANNEL_ID}</yt:channelId>
    <title>Conversion DEX | Iteration Planning 2023-07-17</title>
    <link rel=\"alternate\" href=\"https://www.youtube.com/watch?v=#{video_id}\"/>
    <author>
     <name>GitLab Unfiltered</name>
     <uri>https://www.youtube.com/channel/#{CHANNEL_ID}</uri>
    </author>
    <published>2023-07-17T18:18:45+00:00</published>
    <updated>2023-07-17T18:32:12.167884102+00:00</updated>
   </entry>
</feed>
"''
  binary_data = body_xml.encode('ASCII-8BIT')

  secret_key = ENV['HUB_SIGNATURE_KEY']
  digest = OpenSSL::Digest.new('sha1')
  hmac = OpenSSL::HMAC.hexdigest(digest, secret_key, binary_data)

  uri = URI.parse(ENV['WEBSUB_URL'])
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = (uri.scheme == "https")
  request = Net::HTTP::Post.new(uri.request_uri)
  request['content-type'] = 'application/atom+xml'
  request['x-hub-signature'] = "sha1=#{hmac}"
  request.body = binary_data

  response = http.request(request)
  video_data = {
    'video_id' => video_id,
    'response_code' => response.code,
    'response_text' => response.body
  }

  video_records = File.exist?(VIDEO_IDS_FILE) ? JSON.parse(File.read(VIDEO_IDS_FILE)) : []
  video_records << video_data
  File.write(VIDEO_IDS_FILE, JSON.dump(video_records))
  response
end

def video_sent_to_ocr?(video_id)
  return false unless File.exist?(VIDEO_IDS_FILE)

  video_records = JSON.parse(File.read(VIDEO_IDS_FILE))
  video_records.any? { |record| record['video_id'] == video_id }
end

unless ENV.keys.include?('YOUTUBE_API_KEY')
  puts 'Error: Set the YOUTUBE_API_KEY env var'
  exit
end

unless ENV.keys.include?('HUB_SIGNATURE_KEY')
  puts 'Error: Set the HUB_SIGNATURE_KEY env var'
  exit
end

unless ENV.keys.include?('WEBSUB_URL')
  puts 'Error: Set the WEBSUB_URL env var'
  exit
end
options = {}
parser = OptionParser.new do |opts|
  opts.banner = 'Usage: pullvideos.rb <options>'

  opts.on('-r', '--range RANGE', 'Provide a range of results to be pulled from the API and uploaded. Example: 10,20')
  opts.on('-c', '--confirm', 'Confirm before uploading each video to the scanner.')
  opts.on('-d', '--delete', 'Delete cache file to fetch videos ids from the API.')
  opts.on('-v', '--verbose', 'Print OCR scanner response code.')
  opts.on('-u', '--update', 'Refresh the cache file by fetching new videos from the youtube API.')
  opts.on('-h', '--help', 'Print this help') do
    puts opts
    exit
  end
end
parser.parse!(into: options)

def get_range_from_string(str)
  regex = /^(\d+),(\d+)$/

  match_data = str.match(regex)
  return [] unless match_data

  if match_data[1].to_i < match_data[2].to_i
    [match_data[1].to_i, match_data[2].to_i]
  else
    []
  end
end
if options[:delete]
  if File.exist?(CACHE_FILE)
    File.delete(CACHE_FILE)
    puts 'Cache cleared successfully.'
  else
    puts 'No cache file found.'
  end
else
  if options[:range]
    range = get_range_from_string(options[:range])
    if range.empty?
      puts 'Invalid range provided.'
      exit
    end
    videos_to_upload = fetch_videos(range[0], range[1], options[:update])
  else
    puts 'No range provided. This will upload ALL videos to scanner. Do you wish to proceed? [y/n]'
    answer = gets.chomp
    exit unless answer == 'y'
    videos_to_upload = fetch_videos(nil, nil, options[:update])
  end

  videos_to_upload.each do |video|
    if options[:confirm]
      puts "Do you want to send video \"#{video['title']}\" with id #{video['id']} to scanner? y/n"
      confirmation = gets
      next unless confirmation.rstrip == 'y'
    end
    if video_sent_to_ocr?(video['id'])
      puts "Video #{video['id']} found in #{VIDEO_IDS_FILE} which means it was already scanned, skipping..."
    else
      puts "Sending \"#{video['title']}\" with id #{video['id']} for OCR scanning..."
      response = send_video_to_ocr(video['id'])
      puts "Got a #{response.code} response code."
      puts response.body if options[:verbose]
    end
  end
end
