#!/bin/bash

set -euo pipefail

ENV=dev

# Create topic if it doesn't exist
# gcloud pubsub --project glsec-video-scanner-$dev topics create new-video

# Create buckets if they don't exist
# gcloud storage ...


# Follow this guide to grant permissions:
# https://cloud.google.com/eventarc/docs/run/create-trigger-storage-gcloud#before-you-begin

# Set permissions for websub-api to invoke video-fetcher:
# gcloud run services add-iam-policy-binding video-fetcher \
  # --member='serviceAccount:395356191508-compute@developer.gserviceaccount.com' \
  # --role='roles/run.invoker'

# Set permissions for video-fetcher to invoke secret-matcher
# gcloud run services add-iam-policy-binding secret-matcher \
#   --member='serviceAccount:service-395356191508@gs-project-accounts.iam.gserviceaccount.com' \
#   --role='roles/run.invoker'
# gcloud functions add-invoker-policy-binding secret-matcher \
#   --member=serviceAccount:395356191508-compute@developer.gserviceaccount.com


# Grant permission to create events for cloud storage
# https://cloud.google.com/functions/docs/tutorials/storage
# gcloud projects add-iam-policy-binding $PROJECT_ID \
#   --member serviceAccount:$SERVICE_ACCOUNT \
#   --role roles/pubsub.publisher

# Grant permissions to the buckets
# https://cloud.google.com/storage/docs/access-control/using-iam-permissions#command-line
# gcloud storage buckets add-iam-policy-binding gs://videos-unfiltered-dev 
# --member=serviceAccount:395356191508-compute@developer.gserviceaccount.com
# --role=roles/storage.objectUser
# 
# # gcloud storage buckets add-iam-policy-binding gs://videos-unfiltered-text-extract-dev
# --member=serviceAccount:395356191508-compute@developer.gserviceaccount.com
# --role=roles/storage.objectUser


# Deploy WebSub API
gcloud functions --project "glsec-video-scanner-$ENV" deploy websub-api \
  --region=us-east1 \
  --source=functions/websub-api \
  --entry-point=websub-api \
  --trigger-http \
  --runtime=nodejs20 \
  --gen2 \
  --allow-unauthenticated \
  --env-vars-file functions/shared/.$ENV-env.yml \
  --set-secrets 'YTVS_HMAC_SECRET=websub-hmac-key:latest'

# Deploy Video Fetcher
gcloud functions --project "glsec-video-scanner-$ENV" deploy video-fetcher \
  --region=us-east1 \
  --source=functions/video-fetcher \
  --entry-point=video-fetcher \
  --trigger-topic=new-video \
  --runtime=nodejs20 \
  --gen2 \
  --env-vars-file functions/shared/.$ENV-env.yml

# Deploy Secret Matcher
gcloud functions --project "glsec-video-scanner-$ENV" deploy secret-matcher \
  --region=us-east1 \
  --source=functions/secret-matcher \
  --entry-point=secret-matcher \
  --trigger-event-filters="type=google.cloud.storage.object.v1.finalized" \
  --trigger-event-filters="bucket=videos-unfiltered-text-extract-$ENV" \
  --runtime=nodejs20 \
  --gen2 \
  --env-vars-file functions/shared/.$ENV-env.yml
