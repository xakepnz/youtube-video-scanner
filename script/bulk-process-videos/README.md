# How to use
Start by chaning the WEBSUB_URL value to something that makes sense. Then create a youtube API key and export your `HUB_SIGNATURE_KEY` and `YOUTUBE_API_KEY`. 
```bash
export HUB_SIGNATURE_KEY=Your_Key; export YOUTUBE_API_KEY=Your_key
ruby pullvideos.rb
Usage: pullvideos.rb <options>
    -r, --range RANGE                Provide a range of results to be pulled from the API and uploaded. Example: 10,20
    -c, --confirm                    Get a confirmation prompt before uploading each video to the scanner.
    -d, --delete                     Delete cache file to fetch videos ids from the API.
    -v, --verbose                    Print OCR scanner response code.
    -h, --help                       Print this help
```

### To create a youtube API key:
1. Create a google account
1. Go to https://console.cloud.google.com/apis/credentials
2. Create a project 
3. Use the "Create credentials" button and select API key

### Cache file

I have added a cache file to avoid hitting the youtube API limit too fast. A free API key has 10k "credits" per day and each call costs 100 credits. Therefore, I fetch all the video ID's from the channel on the first run and use the cache until the `-d` switch is used.