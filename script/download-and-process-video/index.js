'use strict'

const ytdl = require('@distube/ytdl-core')
const { request } = require('gaxios')
const { createHmac } = require('node:crypto')

const SCANNER_URL = 'https://websub-api-jsnc2n6dhq-ue.a.run.app' // PROD
// const SCANNER_URL = 'https://websub-api-3ajd33i73a-ue.a.run.app' // DEV

const key = process.env.HMAC_SECRET
const sign = message =>
  'sha1=' + createHmac('sha1', key).update(message).digest('hex')

async function scanVideo (videoId) {
  const body = `<?xml version='1.0' encoding='UTF-8'?>
  <feed xmlns:yt="http://www.youtube.com/xml/schemas/2015" xmlns="http://www.w3.org/2005/Atom"><link rel="hub" href="https://pubsubhubbub.appspot.com"/><link rel="self" href="https://www.youtube.com/xml/feeds/videos.xml?channel_id=UCMtZ0sc1HHNtGGWZFDRTh5A"/><title>YouTube video feed</title><updated>2023-07-17T18:32:12.167884102+00:00</updated><entry>
    <id>yt:video:${videoId}</id>
    <yt:videoId>${videoId}</yt:videoId>
    <yt:channelId>UCMtZ0sc1HHNtGGWZFDRTh5A</yt:channelId>
    <title>Conversion DEX | Iteration Planning 2023-07-17</title>
    <link rel="alternate" href="https://www.youtube.com/watch?v=${videoId}"/>
    <author>
     <name>GitLab Unfiltered</name>
     <uri>https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A</uri>
    </author>
    <published>2023-07-17T18:18:45+00:00</published>
    <updated>2023-07-17T18:32:12.167884102+00:00</updated>
   </entry></feed>`

  const resp = await request({
    url: SCANNER_URL,
    method: 'POST',
    headers: {
      'content-type': 'application/atom+xml',
      'x-hub-signature': sign(body)
    },
    data: Buffer.from(body)
  })

  console.log(`Status: ${resp.status}`)
}

function printHelp () {
  console.log('Usage: HMAC_SECRET=<s3cret> node index.js <videoId>')
}

;(async function main () {
  if (!key) {
    console.log('Please provide HMAC signign key with env variable HMAC_SECRET')
    printHelp()
    process.exit(1)
  }

  const videoId = process.argv.pop()
  if (!ytdl.validateID(videoId)) {
    printHelp()
    process.exit(1)
  }

  await scanVideo(videoId)
})()
