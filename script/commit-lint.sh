#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

git fetch origin main:main
COMMON_ANCESTOR_COMMIT_SHA=$(git merge-base HEAD main)
echo "Checking commit messages from $(git rev-parse HEAD) to $COMMON_ANCESTOR_COMMIT_SHA"
[[ -z "${COMMON_ANCESTOR_COMMIT_SHA}" ]] || npx commitlint --from $COMMON_ANCESTOR_COMMIT_SHA --to HEAD --verbose
