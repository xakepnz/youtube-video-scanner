The following table lists all the tokens Tokinator has found since it started running in July 2022.

| Date       | Description                            | Related links                                  | Bounty Saved                                                      |
| ---------- | -------------------------------------- | ---------------------------------------------- | ----------------------------------------------------------------- |
| yyyy-mm-dd | description of the leak should go here | link to incident issue or other relevant links | bug bounty amount that would have paid if it were reported via H1 |
