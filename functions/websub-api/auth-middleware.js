const { createHmac } = require('node:crypto')
const { HMAC_SECRET } = require('./config')
const createLogger = require('./logging')

module.exports = function create ({ secret = HMAC_SECRET } = {}) {
  /**
   * This middleware checks that incoming POST requests
     have been signed with a shared key.

     See the spec for details:
     http://pubsubhubbub.github.io/PubSubHubbub/pubsubhubbub-core-0.4.html#authednotify

   * @param {*} req Cloud Function request context
   * @param {*} req.headers Header X-Hub-Signature with a sha1 hash of the request body
   * @param {*} res Cloud Function request context
   * @param {*} next Next middleware to call
   * @returns null
   */
  return async function auth (req, res, next) {
    const log = await createLogger()
    const sigHeader = req.get('X-Hub-Signature')
    if (!sigHeader) {
      res.status(401).send()
      return
    }

    // check that the signature matches the expected signature
    const expectedSignature = sigHeader.split('=')[1]
    const hmac = createHmac('sha1', secret)
    hmac.update(req.body)
    const actualSignature = hmac.digest('hex')
    if (expectedSignature !== actualSignature) {
      const entry = log.entry({ httpRequest: req }, {
        actualSignature,
        expectedSignature,
        reqBody: req.body
      })
      log.debug(entry)

      res.status(403).send()
      return
    }

    next()
  }
}
