# Overview

This code implements a subscriber in the context of the [WebSub spec](https://www.w3.org/TR/websub/). It is intended to be called by YouTube's [Notifications API](https://developers.google.com/youtube/v3/guides/push_notifications).

The code implements section [5.3 Hub Verfies Intend of Subscriber](https://www.w3.org/TR/websub/#hub-verifies-intent) and section [7. Content Distribution](https://www.w3.org/TR/websub/#content-distribution) of the spec. The code DOES NOT handle the initial subscription specified in [5.1 Subscriber Sends Subscription Request](https://www.w3.org/TR/websub/#subscriber-sends-subscription-request).

# Deploy to GCP

```
gcloud functions deploy websub-api \
  --region=us-central1 \
  --entry-point=websub-api \
  --trigger-http \
  --runtime=nodejs20 \
  --gen2 \
  --allow-unauthenticated
```