const functions = require('@google-cloud/functions-framework')
const handler = require('./sub-confirmation-middleware')
const authMiddleware = require('./auth-middleware')()
const createWebSub = require('./web-sub-middleware')

const express = require('express')
const app = express()

// example call:
// curl -vvv "localhost:8080/?hub.topic=https://www.youtube.com/xml/feeds/videos.xml%3Fchannel_id%3DUCMtZ0sc1HHNtGGWZFDRTh5A&hub.challenge=14121497117435728779&hub.mode=subscribe&hub.lease_seconds=111"
app.get('/', handler)

// example call:
// curl -vvv  \
// -H 'content-type: application/atom+xml' \
// -H 'x-hub-signature: sha1=29df45085be8591a02ed81add3bea20497fc3c6c' \
// --data-binary '@test/fixtures/post-body1.xml' \
//    localhost:8080/
app.post('/', authMiddleware, createWebSub())

functions.http('websub-api', app)
