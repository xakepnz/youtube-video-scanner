'use strict'

const defaults = {
  TOPIC_ID: 'https://www.youtube.com/xml/feeds/videos.xml?channel_id=UCMtZ0sc1HHNtGGWZFDRTh5A',
  PUBSUB_TOPIC_ID: 'new-video'
}

module.exports = require('rc')('ytvs', defaults)
