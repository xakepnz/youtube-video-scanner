const { Logging } = require('@google-cloud/logging')
const logging = new Logging()

async function createLogger () {
  await _init()

  // Create a LogSync transport, defaulting to `process.stdout`
  return logging.logSync('my-log')
}

async function _init () {
  if (logging.projectId === '{{projectId}}') { await logging.setProjectId() }

  if (!logging.detectedResource) { await logging.setDetectedResource() }
}

module.exports = createLogger
