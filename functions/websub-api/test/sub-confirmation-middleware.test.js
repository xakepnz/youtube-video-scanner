/* eslint-env mocha */
const assert = require('assert')
const httpMocks = require('node-mocks-http')

const config = require('../config')

const handler = require('../sub-confirmation-middleware')

describe('subscription confirmation spec', () => {
  beforeEach(() => {
    this.challenge = '42 is the answer'
    this.req = httpMocks.createRequest({
      method: 'GET',
      url: '/',
      query: {
        'hub.challenge': this.challenge,
        'hub.topic': config.TOPIC_ID,
        'hub.mode': 'subscribe'
      }
    })
    this.res = httpMocks.createResponse()
  })

  it('echoes the challenge', async () => {
    await handler(this.req, this.res)
    assert.equal(this.res._getData(), this.challenge)
  })
})
