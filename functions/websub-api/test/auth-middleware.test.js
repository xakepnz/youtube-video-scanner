/* eslint-env mocha */
const assert = require('assert')
const sinon = require('sinon')
const httpMocks = require('node-mocks-http')
const createAuth = require('../auth-middleware')
const exampleData = require('./fixtures/post-body1')

describe('auth spec', () => {
  beforeEach(() => {
    this.req = httpMocks.createRequest({
      method: 'POST',
      url: '/',
      headers: {
        'x-hub-signature': 'sha1=29df45085be8591a02ed81add3bea20497fc3c6c'
      },
      body: Buffer.from(exampleData)
    })
    this.res = httpMocks.createResponse()
    this.hmac_key = 'supers3cret'
    this.auth = createAuth({ secret: this.hmac_key })
  })

  it('calls next middleware if signature matches', async () => {
    const next = sinon.spy()
    await this.auth(this.req, this.res, next)
    assert(next.calledOnce)
  })

  it('sends 401 if hmac is missing', async () => {
    delete this.req.headers['x-hub-signature']
    await this.auth(this.req, this.res)
    assert.equal(this.res.statusCode, 401)
  })

  it('sends 403 if  hmac doesn\'t match data', async () => {
    this.req.headers['x-hub-signature'] = 'invalid_sig'
    await this.auth(this.req, this.res)
    assert.equal(this.res.statusCode, 403)
  })
})
