/* eslint-env mocha */
const assert = require('assert')
const sinon = require('sinon')
const httpMocks = require('node-mocks-http')
const createWebSub = require('../web-sub-middleware')
const exampleBodyBuffer = require('./fixtures/post-body1')
const exampleBodyJs = require('./fixtures/post-body1-jsObj')
const { PubSub } = require('@google-cloud/pubsub')

describe('web sub spec', () => {
  beforeEach(() => {
    this.req = httpMocks.createRequest({
      method: 'POST',
      url: '/',
      headers: {
        'content-type': 'application/atom+xml'
      },
      body: Buffer.from(exampleBodyBuffer)
    })
    this.res = httpMocks.createResponse()
    this.webSub = createWebSub()
  })

  it('sends 415 on unsupported content-type', async () => {
    this.req.headers['content-type'] = 'foo/bar'
    await this.webSub(this.req, this.res)
    assert.equal(this.res.statusCode, 415)
  })

  it('sends 400 on parser error', async () => {
    this.req.body = Buffer.from('not a valid xml document')
    await this.webSub(this.req, this.res)
    assert.equal(this.res.statusCode, 400)
  })

  it('parses body into object', async () => {
    await this.webSub(this.req, this.res)
    const exampleObj = exampleBodyJs
    assert.deepEqual(this.req.xml, exampleObj)
  })

  it('sends 200 and submits body to pubsub', async () => {
    const pubsub = new PubSub()
    const topic = pubsub.topic('some-topic')
    const topicStub = sinon.stub(topic, 'publishMessage')
    sinon.stub(pubsub, 'topic').callsFake(() => topic)

    await createWebSub({ pubsub })(this.req, this.res)

    const messageObject = {
      data: this.req.xml.feed.entry
    }
    const expectedMessage = {
      data: Buffer.from(JSON.stringify(messageObject), 'utf8')
    }

    assert(topicStub.calledWithExactly(expectedMessage))
    assert(this.res.statusCode, 200)
  })
})
