module.exports = {
  '?xml': {
    '@_encoding': 'UTF-8',
    '@_version': '1.0'
  },
  feed: {
    '@_xmlns': 'http://www.w3.org/2005/Atom',
    '@_xmlns:yt': 'http://www.youtube.com/xml/schemas/2015',
    entry: {
      author: {
        name: 'GitLab Unfiltered',
        uri: 'https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A'
      },
      id: 'yt:video:wK693L7plOM',
      link: {
        '@_href': 'https://www.youtube.com/watch?v=wK693L7plOM',
        '@_rel': 'alternate'
      },
      published: '2023-07-17T18:18:45+00:00',
      title: 'Conversion DEX | Iteration Planning 2023-07-17',
      updated: '2023-07-17T18:32:12.167884102+00:00',
      'yt:channelId': 'UCMtZ0sc1HHNtGGWZFDRTh5A',
      'yt:videoId': 'wK693L7plOM'
    },
    link: [
      {
        '@_href': 'https://pubsubhubbub.appspot.com',
        '@_rel': 'hub'
      },
      {
        '@_href': 'https://www.youtube.com/xml/feeds/videos.xml?channel_id=UCMtZ0sc1HHNtGGWZFDRTh5A',
        '@_rel': 'self'
      }
    ],
    title: 'YouTube video feed',
    updated: '2023-07-17T18:32:12.167884102+00:00'
  }
}
