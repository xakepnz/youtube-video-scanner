const { TOPIC_ID } = require('./config')
const createLogger = require('./logging')

async function handler (req, res) {
  const { 'hub.topic': topic, 'hub.challenge': challenge, 'hub.mode': mode } = req.query || {}
  const log = await createLogger()

  // check parameter
  if (topic !== TOPIC_ID ||
    mode !== 'subscribe' ||
    !challenge) {
    const entry = log.entry({ httpRequest: req }, req.body)
    entry.metadata['content-type'] = req.get('content-type')
    log.debug(entry)

    res.status(400).send('Required parameter missing')
    return
  }

  // challenge needs to be echoed for the subscription to succeed
  res.send(challenge)
}

module.exports = handler
