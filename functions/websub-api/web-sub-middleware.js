const { PubSub } = require('@google-cloud/pubsub')
const createLogger = require('./logging')
const { XMLParser } = require('fast-xml-parser')
const { PUBSUB_TOPIC_ID, PROJECT_ID: projectId } = require('./config')

function webSub ({ pubsub = new PubSub({ projectId }) } = {}) {
  const parser = new XMLParser({
    ignoreAttributes: false,
    attributeNamePrefix: '@_',
    allowBooleanAttributes: true
  })

  /**
   * Parses an atom feed message from the request body and publishes it to PubSub.
   *
   * @param {*} req Cloud Function request context
   * @param {Buffer} req.body Atom feed message
   * @param {*} res Cloud Function response context
   * @returns null
   */
  return async function (req, res, next) {
    const log = await createLogger()
    if (req.get('content-type') !== 'application/atom+xml') {
      res.status(415).send() // 415 = Unsupported Media Type
      return
    }

    let message
    try {
      req.xml = parser.parse(req.body, true)

      log.info(log.entry({ httpRequest: req }, req.xml))

      message = req.xml.feed?.entry
      if (!message) throw new Error('atom message is missing entry property')
    } catch (err) {
      const entry = log.entry({ httpRequest: req }, err.toString())
      log.debug(entry)
      res.status(400).send('Malformed request body')
      return
    }

    const topic = pubsub.topic(PUBSUB_TOPIC_ID)
    const messageObject = {
      data: message
    }
    const messageBuffer = Buffer.from(JSON.stringify(messageObject), 'utf8')
    const msg = { data: messageBuffer }

    const _publishAndCreateTopic = async () => {
      try {
        await topic.publishMessage(msg)
      } catch (err) {
        // TODO: Check if the topic should be created here or somewhere else (terraform?)
        if (err.code === 5) { // topic does not exist
          // create topic and retry
          await pubsub.createTopic(PUBSUB_TOPIC_ID)
          await topic.publishMessage(msg)
        } else {
          throw err
        }
      }
    }

    // Publishes a message
    try {
      await _publishAndCreateTopic()
      res.status(200).send()
    } catch (err) {
      const entry = log.entry({ httpRequest: req }, err.toString())
      log.error(entry)
      res.status(500).send()
    }
  }
}

module.exports = webSub
