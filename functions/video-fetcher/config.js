'use strict'

const defaults = {
  PUBSUB_TOPIC_ID: 'new-video',
  PROJECT_ID: 'dappelt-27ef2190',
  GCS_BUCKET_VIDEO_FILES: 'videos-unfiltered',
  GCS_BUCKET_TEXT_EXTRACT_FILES: 'videos-unfiltered-text-extract'
}

// TODO: make these values configurable
module.exports = require('rc')('ytvs', defaults)
