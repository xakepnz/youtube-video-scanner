'use strict'

class BaseError extends Error {
  get name () {
    return this.constructor.name
  }
}

class FileExistsError extends BaseError {}

class PreconditionMismatchError extends BaseError {}

class EmptyFileError extends BaseError {}

module.exports = {
  FileExistsError,
  PreconditionMismatchError,
  EmptyFileError
}
