'use strict'

const { GCS_BUCKET_TEXT_EXTRACT_FILES, PROJECT_ID } = require('./config')
const Video = require('@google-cloud/video-intelligence')
const { Storage } = require('@google-cloud/storage')
const path = require('path')
const { PassThrough } = require('node:stream')
const { pipeline } = require('node:stream/promises')
const { FileExistsError } = require('./errors')

module.exports = function ({
  video = new Video.VideoIntelligenceServiceClient(),
  textBucketName = GCS_BUCKET_TEXT_EXTRACT_FILES,
  projectId = PROJECT_ID,
  storage = new Storage({ projectId })
} = {}) {
  /**
   * Uploads a video file in a GCS bucket to the Video Intelligence API.
   *
   * @param {string} gcsUri URI of the video file to be uploaded
   * @returns {Video.protos.google.longrunning.Operation} operation
   */
  return async function (gcsUri) {
    const videoFilename = path.basename(gcsUri)

    const textBucket = storage.bucket(`${projectId}-${textBucketName}`)
    const expectedTextFile = textBucket.file(videoFilename + '.json', {
      generation: 0
    })
    const stream = new PassThrough()
    stream.end()

    try {
      // throw if the file exist or create it
      await pipeline(stream, expectedTextFile.createWriteStream())
    } catch (err) {
      if (err.code === 412) {
        // text extract exists, video was already processed
        throw new FileExistsError(
          `${expectedTextFile.cloudStorageURI.href} exists.`
        )
      }
      throw err // unexpected error
    }

    const request = {
      inputUri: gcsUri,
      outputUri: `gs://${projectId}-${textBucketName}/${videoFilename}.json`,
      features: ['TEXT_DETECTION']
    }

    // Detects text in a video
    console.log(
      `submitting request to video intelligence api: ${JSON.stringify(request)}`
    )
    const [operation] = await video.annotateVideo(request)
    return operation
  }
}
