'use strict'

const { pipeline } = require('node:stream/promises')
const defaultYtdl = require('@distube/ytdl-core')
const { Storage } = require('@google-cloud/storage')
const { GCS_BUCKET_VIDEO_FILES, PROJECT_ID } = require('./config')

const { PreconditionMismatchError, EmptyFileError } = require('./errors')

module.exports = function ({
  ytdl = defaultYtdl,
  videoBucketName = GCS_BUCKET_VIDEO_FILES,
  projectId = PROJECT_ID,
  storage = new Storage({ projectId })
} = {}) {
  const videoBucket = storage.bucket(`${projectId}-${videoBucketName}`)

  /**
   * Downloads a video from youtube to a google cloud storage bucket.
   *
   * @param {number} videoId youtube id of the video that is downloaded
   * @returns {File} google cloud storage file object
   */
  return async function (videoId) {
    if (!ytdl.validateID(videoId)) {
      throw new Error(`not a valid YouTube video id: ${videoId}`)
    }

    const file = videoBucket.file(`yt_unfiltered_${videoId}.mp4`, {
      // Setting generation to 0 makes operations succeed only if there is no
      // live version of the object. The operation fails AFTER the write attempt to
      // object has finished, hence this is good to avoid a race condition but does not
      // prevent downloading a video from youtube if the file already exists.
      // See https://cloud.google.com/storage/docs/request-preconditions#special-case
      generation: 0
    })

    // don't download the file again if it already exists
    const fileExists = (await file.exists()).pop()
    if (fileExists) return file

    console.log(`Downloading video ${videoId} to ${file.cloudStorageURI.href}`)

    const info = await ytdl.getInfo(videoId)
    const format = ytdl.chooseFormat(info.formats, {
      quality: 'highestvideo',
      // It seems YT needs some time processing newly uploaded video, in particular
      // higher quality versions. If such videos are requested for download,
      // a placeholder video is returned (the video only shows the text
      // 'This content isn't available.')
      //
      // We do not want to download the placeholder video. This function filters
      // the placeholder video.
      // TODO: are there any other characteristics of the placeholder video.
      filter: f => ![93015, 93016].includes(f.approxDurationMs)
    })

    try {
      await pipeline(
        // Get the highest quality version of the video.
        // filter: 'video' is necessary to downloading videos with "This content is not available"
        // ytdl(videoId, { quality: 'highestvideo', filter: f => ![93015, 93016].includes(f.approxDurationMs) }),
        ytdl(videoId, { quality: 'highestvideo' }),
        file.createWriteStream({
          metadata: {
            itag: format.itag
          }
        })
      )

      const [{ size }] = await file.getMetadata()
      if (size === '0') {
        await file.delete() // delete empty file so that it isn't returned on retrying
        throw new EmptyFileError('Video file is empty')
      }

      console.log(`download completed of ${videoId}`)
    } catch (err) {
      // error 412 is thrown if the precondition "generation: 0" is violoated
      if (err.code === 412) {
        throw new PreconditionMismatchError(
          `Generation precondition mismatch. File already exists: ${file.cloudStorageURI.href}`
        )
      } else {
        throw err
      }
    }

    return file
  }
}
