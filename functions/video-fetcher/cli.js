// const fs = require('fs')
// const util = require('util')
const videoClient = require('./video-client')()

const path = require('path')
const { Storage } = require('@google-cloud/storage')
const storage = new Storage({ projectId: 'dappelt-27ef2190' })
const bucket = storage.bucket('videos-unfiltered')

async function upload (videoFilePath) {
  try {
    await bucket.upload(videoFilePath)
    const filename = path.basename(videoFilePath)
    return 'gs://videos-unfiltered/' + filename
  } catch (err) {
    console.log(err)
    throw err
  }
}

// function print(results) {
//   // Gets annotations for video
//   const textAnnotations = results[0].annotationResults[0].textAnnotations;
//   textAnnotations.forEach(textAnnotation => {
//     console.log(`Text ${textAnnotation.text} occurs at:`);
//     textAnnotation.segments.forEach(segment => {
//       const time = segment.segment;
//       console.log(
//         ` Start: ${time.startTimeOffset.seconds || 0}.${(
//           time.startTimeOffset.nanos / 1e6
//         ).toFixed(0)}s`
//       );
//       console.log(
//         ` End: ${time.endTimeOffset.seconds || 0}.${(
//           time.endTimeOffset.nanos / 1e6
//         ).toFixed(0)}s`
//       );
//       console.log(` Confidence: ${segment.confidence}`);
//       segment.frames.forEach(frame => {
//         const timeOffset = frame.timeOffset;
//         console.log(
//           `Time offset for the frame: ${timeOffset.seconds || 0}` +
//           `.${(timeOffset.nanos / 1e6).toFixed(0)}s`
//         );
//         console.log('Rotated Bounding Box Vertices:');
//         frame.rotatedBoundingBox.vertices.forEach(vertex => {
//           console.log(`Vertex.x:${vertex.x}, Vertex.y:${vertex.y}`);
//         });
//       });
//     });
//   });
// }

async function main () {
  const videoFilePath = process.argv.pop()
  console.log('processing ' + videoFilePath)
  // const filename = path.basename(videoFilePath)
  const gcsUri = await upload(videoFilePath)
  const operation = await videoClient(gcsUri)
  await operation.promise()

  // const dest = '/Users/da/git/sample-data/result/gcp-video-intelligence/' + filename + '.json'
  // console.log('writing results to ' + dest)
  // fs.writeFileSync(dest, JSON.stringify(results[0].annotationResults))
}

if (require.main === module) {
  // we are being called from the terminal
  main()
}

process.on('unhandledRejection', err => {
  console.error(err.message)
  process.exitCode = 1
})
