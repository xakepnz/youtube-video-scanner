# Overview

This code downloads videos from YouTube, stores them in a bucket, and calls the GCP Video Intelligence API to extract all text shown in the videos. The extracted text is also stored in a bucket.

The download is triggered via a message received from a PubSub subscription (see `--trigger-topic` in the deploy command). The expected message format is the XML body of a YouTube push notification. For an example message, see https://developers.google.com/youtube/v3/guides/push_notifications.

# Deploy

```
gcloud functions deploy video-fetcher \
  --region=us-central1 \
  --entry-point=video-fetcher \
  --trigger-topic=new-video \
  --runtime=nodejs20 \
  --gen2
```
