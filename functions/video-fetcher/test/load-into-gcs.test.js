/* eslint-env mocha */
const assert = require('assert')
const sinon = require('sinon')
const ytdl = require('@distube/ytdl-core')
const { Storage } = require('@google-cloud/storage')
const createLoad = require('../load-into-gcs')
const { PassThrough } = require('stream')
const { GcsFileExistsError } = require('./fixtures/errors')

const rickRolled = 'xvFZjo5PgG0'

describe('load into gcs spec', () => {
  beforeEach(() => {
    this.sandbox = sinon.createSandbox()
    this.videoBucketName = 'video-bucket'
    this.expectedVideoFileName = `yt_unfiltered_${rickRolled}.mp4`
    this.fakeFile = {
      createWriteStream: () => new PassThrough(),
      exists: async () => [false],
      delete: async () => {},
      cloudStorageURI: {
        href: `gs://${this.videoBucketName}/${this.expectedVideoFileName}`
      },
      getMetadata: async () => [{ size: '100' }],
      name: this.expectedVideoFileName
    }

    const storage = new Storage()
    const stubBucket = this.sandbox.stub(storage, 'bucket')
    stubBucket.returns({
      file: name => {
        if (name === this.expectedVideoFileName) {
          return this.fakeFile
        } else if (name === this.expectedTextfileName) {
          return this.textFile
        } else {
          console.log(name)
          throw new Error('file not found')
        }
      }
    })
    this.storage = storage

    this.fileExistsError = GcsFileExistsError

    this.expectedItag = '1234'

    const fakeYtdl = () => new PassThrough().end()
    fakeYtdl.getInfo = async () => {
      return { formats: [] }
    }
    fakeYtdl.chooseFormat = () => {
      return { itag: this.expectedItag }
    }
    fakeYtdl.validateID = () => ytdl.validateID
    this.ytdl = fakeYtdl
    this.load = createLoad({ ytdl: fakeYtdl, storage: this.storage })
  })

  afterEach(() => {
    this.sandbox.restore()
  })

  it('downloads from youtube', async () => {
    const myAPI = { ytdl: this.ytdl } // dummy object to make mocks play nicely
    const mock = this.sandbox
      .mock(myAPI)
      .expects('ytdl')
      .once()
      .withArgs(rickRolled)
      .returns(new PassThrough().end())

    this.load = createLoad({ ytdl: myAPI.ytdl, storage: this.storage })
    const actual = await this.load(rickRolled)

    mock.verify()
    assert.deepStrictEqual(actual, this.fakeFile)
  })

  it.skip('throws an error if no suitable format is available', async () => {
    const stream = new PassThrough()
    const stub = sinon.stub(this.myAPI, 'ytdl').callsFake(() => {
      setImmediate(() => {
        stream.emit('error', new Error('No such format found: highest'))
        stream.end()
      })

      return stream
    })

    this.load = createLoad({ ytdl: stub, storage: this.storage })

    await assert.rejects(this.load.bind(null, rickRolled), {
      name: 'Error',
      message: 'No such format found: highest'
    })
  })

  it('does not race when downloading a video', async () => {
    sinon.stub(this.fakeFile, 'createWriteStream').throws(this.fileExistsError)
    await assert.rejects(this.load.bind(null, rickRolled), {
      name: 'PreconditionMismatchError',
      message: `Generation precondition mismatch. File already exists: ${this.fakeFile.cloudStorageURI.href}`
    })
  })

  it('returns video file if it exists in bucket', async () => {
    this.fakeFile.exists = async () => [true]
    const file = await this.load(rickRolled)
    assert.deepStrictEqual(file, this.fakeFile)
  })

  it('does not call YT if file exists in bucket', async () => {
    const spy = this.sandbox.spy(this, 'ytdl')
    this.fakeFile.exists = async () => [true]
    await this.load(rickRolled)
    assert(spy.notCalled)
  })

  it('sets itag in object metadata', async () => {
    const spy = sinon.spy(this.fakeFile, 'createWriteStream')
    await this.load(rickRolled)
    assert(spy.calledWith({ metadata: { itag: this.expectedItag } }))
  })

  it('uses the buckets name from the environment variables', async () => {
    const projectId = 'my-project-id'
    this.load = createLoad({
      ytdl,
      storage: this.storage,
      projectId,
      videoBucketName: this.videoBucketName
    })
    assert(
      this.storage.bucket.calledWith(`${projectId}-${this.videoBucketName}`)
    )
  })
})
