/* eslint-env mocha */
const assert = require('assert')
const sinon = require('sinon')
const { PassThrough } = require('stream')

const createLoad = require('../video-client')
const { GcsFileExistsError } = require('./fixtures/errors')

const rickRolled = 'xvFZjo5PgG0'

describe('video client spec', function () {
  beforeEach(() => {
    this.videoIntelligenceStub = sinon.stub()
    this.storageStub = sinon.stub()
    this.bucketStub = sinon.stub()

    this.projectId = 'my-project'
    this.textBucketName = 'text-bucket'
    this.videoFileName = `yt_unfiltered_${rickRolled}.mp4`
    this.textFileName = this.videoFileName + '.json'
    this.textFile = {
      createWriteStream: () => new PassThrough(),
      cloudStorageURI: {
        href: `gs://${this.textBucketName}/${this.textFileName}`
      },
      name: this.textFileName
    }

    this.videoIntelligenceStub.annotateVideo = sinon.stub().returns([{}])
    this.storageStub.bucket = sinon.stub().returns(this.bucketStub)
    this.bucketStub.file = sinon.stub().returns(this.textFile)

    this.videoClient = createLoad({
      video: this.videoIntelligenceStub,
      textBucketName: this.textBucketName,
      projectId: this.projectId,
      storage: this.storageStub
    })

    this.fileExistsError = GcsFileExistsError
  })

  afterEach(() => {
    sinon.restore()
  })

  it('should call VideoIntelligenceServiceClient.annotateVideo', async () => {
    const videoURI = `gs://video-bucket/${this.videoFileName}`
    await this.videoClient(videoURI)
    assert(this.videoIntelligenceStub.annotateVideo.calledOnce)
    const expectedArgs = {
      inputUri: videoURI,
      outputUri: `gs://${this.projectId}-${this.textBucketName}/${this.textFileName}`,
      features: ['TEXT_DETECTION']
    }

    assert.deepStrictEqual(
      this.videoIntelligenceStub.annotateVideo.getCall(0).args,
      [expectedArgs]
    )
  })

  it('should throw FileExistsError if text extract already exists', async () => {
    sinon.stub(this.textFile, 'createWriteStream').throws(this.fileExistsError)
    const videoURI = `gs://video-bucket/${this.videoFileName}`

    await assert.rejects(this.videoClient.bind(null, videoURI), {
      name: 'FileExistsError',
      message: `gs://${this.textBucketName}/${this.textFileName} exists.`
    })
  })
})
