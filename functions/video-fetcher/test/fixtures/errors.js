module.exports = {
  GcsFileExistsError: {
    code: 412,
    message: 'At least one of the pre-conditions you specified did not hold.',
    errors: [
      {
        message:
          'At least one of the pre-conditions you specified did not hold.',
        domain: 'global',
        reason: 'conditionNotMet',
        locationType: 'header',
        location: 'If-Match'
      }
    ]
  }
}
