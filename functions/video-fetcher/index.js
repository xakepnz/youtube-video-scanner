const functions = require('@google-cloud/functions-framework')
const { FileExistsError, EmptyFileError } = require('./errors')
const promiseRetry = require('promise-retry')
const loadVid = require('./load-into-gcs')()
const videoClient = require('./video-client')()

// Register a CloudEvent function with the Functions Framework
functions.cloudEvent('video-fetcher', async cloudEvent => {
  console.log(JSON.stringify(cloudEvent.data))

  const obj = _parseMessage(cloudEvent.data.message)
  const vidId = obj['yt:videoId']

  if (!vidId) {
    throw new Error(`video id missing in cloud event: ${JSON.stringify(obj)}`)
  }

  try {
    const gcsFile = await _downloadRetry(vidId)
    await videoClient(gcsFile.cloudStorageURI.href)
  } catch (err) {
    if (err instanceof FileExistsError) {
      console.log(`Skipping processing of ${vidId}: ${err.message}`)
      return
    }

    console.log(`Unexpected error while downloading ${vidId}: ${err.message}`)
  }
})

async function _downloadRetry (vidId) {
  const opts = {
    minTimeout: 5000,
    retries: 2
  }
  return await promiseRetry(retry => {
    return loadVid(vidId).catch(err => {
      if (err instanceof EmptyFileError) {
        retry(err)
      } else {
        throw err
      }
    })
  }, opts)
}

function _parseMessage (msg) {
  const jsonStr = Buffer.from(msg.data, 'base64').toString()
  return JSON.parse(jsonStr).data
}
