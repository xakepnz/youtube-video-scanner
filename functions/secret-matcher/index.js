const functions = require('@google-cloud/functions-framework')

const { createHandler } = require('./handler')
const handler = createHandler()

functions.cloudEvent('secret-matcher', async event => {
  console.log(JSON.stringify(event.data))
  const file = JSON.parse(
    Buffer.from(event.data.message.data, 'base64').toString()
  )
  console.log(`Search for secrets in ${file.name} `)
  try {
    await handler(file)
  } catch (error) {
    console.log(`Unexpected error while processing ${file.name}: ${error}`)
  }
})

async function main () {
  const filename = process.argv.pop()
  const file = {
    bucket: 'glsec-video-scanner-live-videos-unfiltered-text-extract',
    name: filename
  }
  console.log(`processing ${filename}`)
  await handler(file)
}

if (require.main === module) {
  main().catch(e => console.log(e))
}
