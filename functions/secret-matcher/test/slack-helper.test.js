'use strict'

/* eslint-env mocha */
'use strict'

const sinon = require('sinon')
const assert = require('node:assert')
const create = require('../slack-helper')

describe('slack helper spec', function () {
  this.beforeEach(function () {
    this.spy = sinon.spy()
    this.expectedUrl = 'https://hooks.slack.com/services/foo/bar/baz'
    this.sendAlertToSlack = create({
      fetch: this.spy,
      slackWebhook: this.expectedUrl
    })

    process.env.NODE_ENV = 'production'
  })

  it('sends an alert to web hook url', async function () {
    await this.sendAlertToSlack()
    assert(this.spy.calledWith(this.expectedUrl))
  })

  it('sends an alert to channel #security-research-video-scanner-alerts', async function () {
    const expectedFilename = 'yt_unfiltered_123_456.mp4.json'
    await this.sendAlertToSlack(expectedFilename)

    const actualBody = JSON.parse(this.spy.args[0][1].body)
    assert.equal(actualBody.channel, '#security-research-video-scanner-alerts')
    assert(
      actualBody.text.includes(expectedFilename),
      'Expected the filename to be part of the Slack message'
    )
  })
})
