/* eslint-env mocha */
'use strict'

const sinon = require('sinon')
const assert = require('node:assert')
const { createHandler, detectSecrets } = require('../handler')
const { PassThrough } = require('node:stream')
const { Storage } = require('@google-cloud/storage')

describe('handler spec', function () {
  beforeEach(() => {
    this.videoIntelligenceApiResponse = {
      textAnnotations: [
        {
          text: 'not a secret',
          segments: [
            {
              segment: {
                start_time_offset: {
                  seconds: '84',
                  nanos: 400000000
                },
                end_time_offset: {
                  seconds: '86'
                }
              }
            }
          ]
        }
      ]
    }
  })

  describe('createHandler', function () {
    beforeEach(() => {
      const { videoIntelligenceApiResponse } = this.parent
      videoIntelligenceApiResponse.textAnnotations[0].text =
        'glpat-01234567890123456789'
      const stream = new PassThrough()
      stream.write(JSON.stringify(this.parent.videoIntelligenceApiResponse))
      stream.end()
      this.fakeFile = {
        createReadStream: () => stream,
        getMetadata: () => Promise.resolve([{ size: '1234' }])
      }
      this.storage = new Storage()
      const stubBucket = sinon.stub(this.storage, 'bucket')
      stubBucket.returns({ file: () => this.fakeFile })
    })

    it('creates a function', function () {
      assert.equal(typeof createHandler(), 'function')
    })

    it('sends an alert to slack if a secret is detected', async () => {
      const alertToSlackStub = sinon.stub()
      const handler = createHandler({
        storage: this.storage,
        sendAlertToSlack: alertToSlackStub
      })
      const expectedFile = { name: 'foo.json' }
      await handler(expectedFile)
      assert(
        alertToSlackStub.calledWith(expectedFile.name),
        'expected alert to slack to be called'
      )
    })

    it('skips if file size is 0', async () => {
      this.fakeFile.getMetadata = () => Promise.resolve([{ size: '0' }])
      this.fakeFile.createReadStream = () => {
        throw new Error('createReadStream() should not have been called')
      }
      const handler = createHandler({ storage: this.storage })
      await assert.doesNotReject(handler({ name: 'foo.json' }))
    })
  })

  describe('detectSecrets', function () {
    beforeEach(() => {
      this.spy = sinon.spy(console, 'log')

      this.apiResp = this.parent.videoIntelligenceApiResponse

      this.stream = new PassThrough()
      setImmediate(() => {
        this.stream.emit('data', Buffer.from(JSON.stringify(this.apiResp)))
        this.stream.end()
      })
    })

    afterEach(() => {
      this.spy.restore()
    })

    const data = [
      {
        data: 'glpat-01234567890123456789',
        label: 'Personal Access Token'
      },
      {
        data: Buffer.from('glpat-01234567890123456789').toString('base64'),
        label: 'Base64 Personal Access Token'
      },
      {
        data: 'glft-012345678901234567890',
        label: 'Feed Token'
      },
      {
        data: 'feed_token=01234567890123456789',
        label: 'Old Feed Token'
      },
      {
        data: 'glptt-aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
        label: 'Pipeline Trigger Token'
      },
      {
        data: 'GR1348941aaaaaaaaaaaaaaaaaaaa',
        label: 'Runner Registration Token'
      },
      {
        data: '_gitlab_session=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa;',
        label: 'GitLab Session Cookie'
      },
      {
        data: 'omamori_pat_aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
        label: 'Omamori Personal Access Token'
      },
      {
        data: 'glagent-1234567890abcdef1234567890abcdef1234567890abcdef12',
        label: 'GitLab Agent Token'
        // },
        // {
        //   data: '+91 (76961) 75833',
        //   label: 'Phone Number'
        // }, {
        //   data: '+91 84520 06407',
        //   label: 'Phone Number'
        // }, {
        //   data: 'foo@gitlab.com',
        //   label: 'Email'
      },
      {
        data: 'gloas-1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcd_-',
        label: 'GitLab OAuth Application Secret'
      },
      {
        data: 'sk-1234567890abcdefghijT3BlbkFJ1234567890abcdefghij',
        label: 'OpenAI API Key'
      },
      {
        data: 'ls__1234567890abcdef1234567890abcdef',
        label: 'Langchain API key'
      }
    ]

    data.map(entry =>
      it(`detects ${entry.label}`, async () => {
        this.apiResp.textAnnotations[0].text = entry.data
        const expectedFileName = 'dummyfile'
        const expectedSeconds =
          this.apiResp.textAnnotations[0].segments[0].segment.start_time_offset
            .seconds
        const secretFound = await detectSecrets(this.stream, expectedFileName)
        assert(secretFound === true)
        assert(
          this.spy.calledWith(
            `SECRET FOUND in ${expectedFileName} at ${expectedSeconds} seconds: "${entry.data}"`
          )
        )
      })
    )

    it('does not match non-secrets ', async () => {
      this.apiResp.textAnnotations[0].text = 'also not a secret'
      const expectedFileName = 'dummyfile'
      const secretFound = await detectSecrets(this.stream, expectedFileName)
      assert(secretFound === false)
      assert(this.spy.calledWith(`No secret found in ${expectedFileName}`))
    })

    it('matches approximately', async () => {
      // Explanation of why the test string matches:
      // The approx. matching algorithm will replace the first 2 occurrences of _
      // with an alphanumerical character. The resulting string matches the
      // PAT regex.
      this.apiResp.textAnnotations[0].text = 'glpat-012345678_901_234567'
      const expectedFileName = 'dummyfile'
      const secretFound = await detectSecrets(this.stream, expectedFileName)
      assert(secretFound === true)
    })

    it('does not match if cost is exceeded', async () => {
      // Explanation of why the test string does not match:
      // underscore (_) is not in the character set of personal access tokens
      // The approx. matching algorithm will replace the first 2 occurrences of _
      // with a valid character. At the time it is processing the 3rd occurrence,
      // it has reached the maxCost (3) and cannot replace the 4th underscore.
      // At this point, it has matched 19 valid characters, which are:
      //    012345678?901?23?56 (? denotes any valid character)
      //
      // Since the regex requires 20 alphanumerical characters, one character
      // is missing and the match fails.
      this.apiResp.textAnnotations[0].text = 'glpat-012345678_901_23_56789'
      const expectedFileName = 'dummyfile'
      const secretFound = await detectSecrets(this.stream, expectedFileName)
      assert(secretFound === false)
    })
  })
})
