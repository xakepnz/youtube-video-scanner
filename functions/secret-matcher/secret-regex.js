'use strict'

// Disable due to false positives
//   // PHONE_NUMBER: /\+\d{1,4}(?:[ -]\(?\d{2,}\)?)(?:[ -]\(?\d{2,}\)?)+/,
//   // EMAIL: /[\w+\-.]+@[a-z\d\-.]+\.[a-z]+/i

const alnum = 'a-zA-Z0-9'

module.exports = [
  {
    name: 'OLD_FEED_TOKEN',
    regex: `feed_token=([${alnum}-]{20})`,
    cost: 1
  },
  {
    name: 'FEED_TOKEN',
    regex: `glft-[${alnum}-]{20}`, // no need to match for glft-[${alnum}]{64}-\\d+
    cost: 1
  },
  {
    name: 'PERSONAL_ACCESS_TOKEN',
    regex: `glpat-[${alnum}-]{20}`,
    cost: 2
  },
  {
    name: 'BASE64_PERSONAL_ACCESS_TOKEN',
    regex: `Z2xwYXQt[${alnum}+/=_-]{27,28}`,
    cost: 1
  },
  {
    name: 'PIPELINE_TRIGGER_TOKEN',
    regex: `glptt-[${alnum}-]{40}`,
    cost: 1
  },
  {
    name: 'RUNNER_REGISTRATION_TOKEN',
    regex: `GR1348941[${alnum}-]{20}`,
    cost: 2
  },
  {
    name: 'GITLAB_SESSION_COOKIE',
    regex: `_gitlab_session=[${alnum}-]{32};`,
    cost: 1
  },
  {
    name: 'OMAMORI_PERSONAL_ACCESS_TOKEN',
    regex: `omamori_pat_[${alnum}]{52}`,
    cost: 1
  },
  {
    name: 'GITLAB_AGENT_TOKEN',
    regex: `glagent-[${alnum}-]{50}`,
    cost: 1
  },
  {
    name: 'OAUTH_APPLICATION_SECRET',
    regex: `gloas-[${alnum}_-]{64}`,
    cost: 1
  },
  {
    name: 'OPENAI_API_KEY',
    // T3BlbkFJ is base64-encoded string "OpenAI"
    regex: `sk-[${alnum}]{20}T3BlbkFJ[${alnum}]{20}`,
    cost: 2
  },
  {
    name: 'LANGCHAIN_API_KEY',
    regex: 'ls__[a-f0-9]{32}',
    cost: 1
  }
]
