'use strict'

const { detectSecrets } = require('../handler')
const fs = require('fs')
const path = require('path')
const chalk = require('chalk')

// usage: node script/scan_local_api_response.js <path to sample data directory>
// example: node script/scan_local_api_response.js /Users/da/git/sample-data/

async function main () {
  const sampleDataDir = process.argv[2]
  const metadataFile = path.join(
    sampleDataDir,
    'videos-containing-leaked-tokens/data.json'
  )
  const apiResponseDir = path.join(
    sampleDataDir,
    'result/gcp-video-intelligence'
  )
  const files = fs.readdirSync(apiResponseDir)

  // Example entry of the metadata file
  // {
  //   "videoId": "jyG3vzJqfuo",
  //   "secrets": [
  //     {
  //       "token": "GR1348941C3NTs-nNLFV4t5gVZWoF",
  //       "type": "runner_registration_token",
  //       "startAt": "1:12",
  //       "endAt": ""
  //     }
  //   ],
  //   "detected": {
  //     "type": "h1report",
  //     "link": "https://hackerone.com/reports/1976398"
  //   }
  // }
  const metadata = JSON.parse(fs.readFileSync(metadataFile))
  for (const e of metadata) {
    if (
      // only check secrets with `glpat-` or `GR1348941`
      !e.secrets.some(
        s =>
          (s.type === 'personal_access_token' &&
            s.token.startsWith('glpat-')) ||
          (s.type === 'runner_registration_token' &&
            s.token.startsWith('GR1348941'))
      )
    ) {
      continue
    }

    console.log(chalk.yellow('PROCESSING ENTRY:'))
    console.log(e)

    const f = files.filter(file => file.includes(e.videoId)).pop()
    const stream = fs.createReadStream(path.join(apiResponseDir, f))
    const secretFound = await detectSecrets(stream, f)
    if (secretFound) {
      console.log(
        chalk.green(
          'TEST POTENTIALLY SUCCESSFUL. ' +
            'PLEASE INSPECT IF THE MATCHED STRING IS THE EXPECTED SECRET.'
        )
      )
    } else {
      console.log(chalk.red('TEST FAILED'))
    }
  }
}

main().catch(err => console.log(err))
