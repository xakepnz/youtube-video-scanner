# Overview

This code searches text extracted from videos via the GCP Video Intelligence API for secrets. The code is called whenever a new file is added to a bucket. The bucket is specified by the parameter `trigger-event-filters=` in the deploy command. For the matched patterns, see [./secret-regex.js](./secret-regex.js)

# Installation

```
npm config set ...
```

# How to add regular expressions

`secret-matcher` matches for an extendable list of regular expressions. Regular expressions are defined in [./secret-regex.js](secret-regex.js). The regex syntax, including how cost is calculated, is documented at <https://laurikari.net/tre/documentation/regex-syntax/>. Note that predefined character classes don't seem to work reliably. Do not use them. Use custom character classes instead (e.g. to match alphanumerical characters, don't use `[:alphanum:]`, but `[0-9a-zA-Z]`).

Be aware that Cloud Functions imposes a [function timeout](https://cloud.google.com/functions/docs/configuring/timeout), which is 9 minutes currently. Within this time limit, all regular expressions must have been evaluated on the given text extract. When adding regular expressions, make sure that the time limit is not exceeded.

Any new regexes should be tested using this [script](script/scan_local_api_response.js). The script will run all regexes defined in [./secret-regex.js](secret-regex.js) against a collection of videos that leaked secrets in the past. The database of videos is available in the [Sample Data](https://gitlab.com/gitlab-com/gl-security/security-research/video-scanner/sample-data) project.

# Deploy

On your local host:
```
IMAGE_NAME="us-east1-docker.pkg.dev/glsec-video-scanner-dev/cloud-run-images/secret-matcher"

# Build the image. 
# 
# REGISTRY_READ_TOKEN is an env variable providing access to the package registry of
# https://gitlab.com/gitlab-com/gl-security/security-research/video-scanner/tre-node-bindings/
REGISTRY_READ_TOKEN=$(op item get "My GitLab PAT" --field credential)
docker build --secret id=REGISTRY_READ_TOKEN . -t $IMAGE_NAME

# Push image
gcloud auth configure-docker us-east1-docker.pkg.dev
docker push $IMAGE_NAME

# Deploy image
gcloud run deploy --image $IMAGE_NAME
```
