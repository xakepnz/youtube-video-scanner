'use strict'
const { chain } = require('stream-chain')
const { streamArray } = require('stream-json/streamers/StreamArray')
const Pick = require('stream-json/filters/Pick')
const { Storage } = require('@google-cloud/storage')
const { PROJECT_ID: projectId } = require('./config')
const PATTERNS = require('./secret-regex')
const defaultSendAlertToSlack = require('./slack-helper')()
const tre = require('@gitlab-com/tre-node-bindings')

function createHandler ({
  storage = new Storage({ projectId }),
  sendAlertToSlack = defaultSendAlertToSlack
} = {}) {
  return async function handler (file) {
    const fileObj = storage.bucket(file.bucket).file(file.name)

    const [{ size }] = await fileObj.getMetadata()

    if (parseInt(size) === 0) {
      console.log(`Skipping empty file: ${file.name}`)
      return
    }

    // TODO: check if file contains an API error

    const isSecretDetected = await detectSecrets(
      fileObj.createReadStream(),
      file.name
    )
    if (isSecretDetected) await sendAlertToSlack(file.name)
  }
}

async function detectSecrets (fileReadStream, filename) {
  const pipeline = chain([
    fileReadStream,
    Pick.withParser({ filter: /text/ }),
    streamArray()
  ])

  let secretFound = false
  for await (const { value } of pipeline) {
    Object.values(PATTERNS).forEach(pattern => {
      if (tre.regaexec(pattern.regex, value.text, pattern.cost)) {
        const timestamp = value.segments[0].segment?.start_time_offset?.seconds
        console.log(
          `SECRET FOUND in ${filename} at ${timestamp} seconds: ${JSON.stringify(
            value.text
          )}`
        )
        secretFound = true
      }
    })
  }

  if (!secretFound) {
    console.log(`No secret found in ${filename}`)
  }

  return secretFound
}

module.exports = {
  createHandler,
  detectSecrets
}
