'use strict'
const defaultFetch = require('node-fetch')
const { SLACK_WEBHOOK: defaultSlackWebhook } = require('./config')

const LOG_URL =
  'https://console.cloud.google.com/functions/details/us-east1/secret-matcher' +
  '?env=gen2&project=glsec-video-scanner-live&tab=logs'

module.exports = function ({
  fetch = defaultFetch,
  slackWebhook = defaultSlackWebhook
} = {}) {
  return async function sendAlertToSlack (filename) {
    if (process.env.NODE_ENV !== 'production') {
      return
    }

    console.log('Sending notification to Slack')

    const payload = {
      channel: '#security-research-video-scanner-alerts',
      username: 'YouTube Video Scanner',
      text: `A secret was found in the video \`${filename}\`. Check the logs for more details ${LOG_URL}`,
      icon_emoji: ':youtube:'
    }

    await fetch(slackWebhook, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    })
  }
}
