> Scans public videos on [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) for access tokens, PII, etc.

> Read more about the motivation behind the video scanner and the implementation challenges in this [blog](https://about.gitlab.com/blog/2024/02/29/how-to-detecting-secrets-in-video-content/).

# Overview

![diagram](docs/diagram.svg)

This project houses the implementation of the core components of the YouTube video scanner. The components are implemented as GCP Cloud Function. The components are:
- [WebSub API](functions/websub-api/)
- [Video Fetcher](functions/video-fetcher/)
- [Secret Matcher](functions/secret-matcher/)

Other GCP APIs that are used in this project:
- **GCP Cloud Scheduler** is used to implement `Subscriber` (Note: will be moved to a cloud function)
- **GCP Cloud Storage** is used to store video files and the text extracted from the videos
- **GCP Cloud PubSub** is used to decouple `WebSub API` from `Video Fetcher`. `WebSub API` publishes a message to a PubSub topic for every new video published on GitLab Unfiltered. `Video Fetcher` is subscribed to this topic and will download the video.
- **GCP Video Intelligence API** is used to extract text from videos.

# Dev

See individual [functions](./functions/) directories for development instructions.

# Ops

See the terraform [config](https://gitlab.com/gitlab-com/gl-security/security-research/security-research-terraform-config#youtube-video-scanner) for deploy instructions.

# Contribute

See [CONTRIBUTING.md](docs/CONTRIBUTING.md).
